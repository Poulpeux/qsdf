Backend 

* Port par défault :9000
1. Se placer dans le répertoire <chemin-appli>/server
2. Lancer la commande :  *gradlew bootRun*
3. Le serveur back end est fonctionnelle quand il arrive à 75% (il est en attente des requêtes)

Front-end 

* port par default :8080
1. Se place dans le répertoire <chemin-appli>/client
2. Dépendance à ne faire qu'une fois
2. *brew install yarn*
3. *yarn global add @vue/cli@3.1.5*
4. *yarn add axios@0.18.0 vuejs-logger@1.5.3*
5. Lancer le serveur
5. *yarn serve*

Prérequis :
- Java v11
- npm 6.13
- Gradle 6.3
