module.exports = {
  pages: {
    'index': {
      entry: './src/pages/Home/main.js',
      template: 'public/log.html',
      title: 'Home',
      chunks: [ 'chunk-vendors', 'chunk-common', 'index' ]
    },
    'about': {
      entry: './src/pages/About/main.js',
      template: 'public/index.html',
      title: 'About',
      chunks: [ 'chunk-vendors', 'chunk-common', 'about' ]
    },
    'Dashboard': {
      entry: './src/pages/Dashboard/main.js',
      template: 'public/index.html',
      title: 'Dashboard',
      chunks: [ 'chunk-vendors', 'chunk-common', 'Dashboard' ]
    },
    'Ss': {
      entry: './src/pages/Ss/main.js',
      template: 'public/index.html',
      title: 'Ss',
      chunks: [ 'chunk-vendors', 'chunk-common', 'Ss' ]
    },
    'Calendar': {
      entry: './src/pages/Calendar/main.js',
      template: 'public/index.html',
      title: 'Calendar',
      chunks: [ 'chunk-vendors', 'chunk-common', 'Calendar' ]
    },
    'CaiView': {
      entry: './src/pages/CaiView/main.js',
      template: 'public/index.html',
      title: 'CaiView',
      chunks: [ 'chunk-vendors', 'chunk-common', 'CaiView' ]
    },
    'CreateCai': {
      entry: './src/pages/CreateCai/main.js',
      template: 'public/index.html',
      title: 'CreateCai',
      chunks: [ 'chunk-vendors', 'chunk-common', 'CreateCai' ]
    },
    'IEC': {
      entry: './src/pages/IEC/main.js',
      template: 'public/index.html',
      title: 'About',
      chunks: [ 'chunk-vendors', 'chunk-common', 'IEC' ]
    }
  }
}
