import axios from 'axios'

const SERVER_URL = 'http://localhost:9000'

const instance = axios.create({
  baseURL: SERVER_URL,
  timeout: 1000
})
// allTheDates: [{"title": "Etape 1","description": null,"datej": "08/05/2020"}]
export default {
  // (C)reate
  createNew: (text) => instance.post('CAI', text),
  // (R)ead
  getAll: () => instance.get('CAIs', {
    transformResponse: [function (data) {
      return data ? JSON.parse(data) : data
    }]
  }),
  // (R)ead
  getAllAlert: () => instance.get('alertes', {
    transformResponse: [function (data) {
      return data ? JSON.parse(data) : data
    }]
  }),
  // GetOneCai
  getOne: (id) => instance.get('CAI/' + id, {
    transformResponse: [function (data) {
      return data ? JSON.parse(data) : data
    }]
  }),
  // GetAllAlertOfACai
  GetAllAlertOfACai: (id) => instance.get('CAI/' + id + '/alertes', {
    transformResponse: [function (data) {
      return data ? JSON.parse(data) : data
    }]
  }),
  // GetAllJalonOfACai
  GetAllJalonOfACai: (id) => instance.get('CAI/' + id + '/jalons', {
    transformResponse: [function (data) {
      return data ? JSON.parse(data) : data
    }]
  }),
  // (U)pdate
  updateForId: (id, text, completed) => instance.put('todos/' + id, { title: text, completed: completed }),
  // (U)pdate
  updateCAI: (id, text) => instance.put('CAIs/' + id, text),
  // (U)pdate
  updateCAIStatus: (id, text) => instance.put('CAIs/' + id, text),
  // (D)elete
  removeForId: (id) => instance.delete('todos/' + id),
  // (D)elete
  removeJalon: (id) => instance.delete('jalon/' + id),
  // Jalon
  addNJalon: (t) => instance.post('liloulo/', t)
}
