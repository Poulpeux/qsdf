package com.soap.springbootvue;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

  
@Entity(name = "CAIJalon")
@Table(name = "caijalon")
@Data  
@NoArgsConstructor 
 @Getter @Setter
public class CAIJalon {  
      
  public CAIJalon(Date dateJalon, String TitleJalon, String DescriptionJalon) {
    datej = dateJalon;
    description = DescriptionJalon;
    title = TitleJalon;
  }
  
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jal_seq")
  @SequenceGenerator(initialValue = 1, name = "jal_seq", sequenceName = "jalon_sequence") 
  private Long id;  

  @NonNull
  private String title;

  private String description;

  public String status;

  @NonNull
  @JsonFormat(pattern = "yyyy-MM-dd")
    private Date datej;  
    private Long cai;
    private String caiTitle;
  @Override
  public int hashCode() {
      return id ==null ? 0: id.intValue() ;
  }

}