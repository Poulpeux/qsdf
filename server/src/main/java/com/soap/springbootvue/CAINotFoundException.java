package com.soap.springbootvue;  
  
class CAINotFoundException extends RuntimeException {

  CAINotFoundException(Long id) {
    super("Could not find CAI " + id);
  }
}