package com.soap.springbootvue;  
  import java.util.*; 
import org.springframework.web.bind.annotation.GetMapping; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.time.temporal.ChronoUnit;

 
@RestController  
public class AlertesController {  
      
   private final CAIRepository repository;
   private final CAIJalonRepository jalonRepository;
  private final int nbJoursWarning = 5;
  AlertesController(CAIRepository repository, CAIJalonRepository jalonRepository) {
    this.repository = repository;
    this.jalonRepository=jalonRepository;
  }
   
   //type, nom projet, Id projet, nb jours 
   //Le projet ID est en retard de XXX jours 
   
   @GetMapping("/alertes")
  List<Alerte> all() {
    
    List<CAIJalon> jalons= jalonRepository.findAll();

    //date d'arrivée est dans qlq jours --> alerte jaune
    //date d'arrivée est dépassé --> alerte rouge
   Iterator<CAIJalon> iter 
            = jalons.iterator(); 
   List<Alerte> result=new ArrayList<Alerte>();
    Long millis = System.currentTimeMillis();
    Date aujourdhui = new Date(millis);

    while (iter.hasNext()) { 
           CAIJalon  jalon = iter.next();
           boolean b = new String(jalon.status).equals("ko");
            if(jalon.getDatej() != null && b){
              long difference = jalon.getDatej().getTime() - aujourdhui.getTime() ;
              int nbJoursDiff = (int)(difference / (1000*60*60*24));
              System.out.println(jalon.getTitle() + " Danger, nbjour=" + nbJoursDiff); 

              if (  nbJoursDiff < 1) {
                    Alerte a=new Alerte(jalon.getCai(), jalon.getTitle(), jalon.getCaiTitle(), "Danger", nbJoursDiff);
                    result.add(a);
                    System.out.println(jalon.getCaiTitle() + jalon.getTitle() + " Danger, nbjour=" + nbJoursDiff); 

                } else if ( nbJoursDiff < nbJoursWarning ) {
                    Alerte a=new Alerte(jalon.getCai(), jalon.getTitle(), jalon.getCaiTitle(), "Warning", nbJoursDiff );
                    result.add(a);
                    System.out.println(jalon.getCaiTitle() + jalon.getTitle() + " Warning, nbjour=" + nbJoursDiff); 
                }
            } 
        } 
    return result;
  }


  @GetMapping("CAI/{id}/alertes")
  List<Alerte> getAlerteByCAIId(@PathVariable Long id) {
    
    List<CAIJalon> jalons= jalonRepository.getJalonByCAIId(id);

   Iterator<CAIJalon> iter 
            = jalons.iterator(); 
   List<Alerte> result=new ArrayList<Alerte>();
    Long millis = System.currentTimeMillis();
    Date aujourdhui = new Date(millis);

    while (iter.hasNext()) { 
           CAIJalon  jalon = iter.next();
           boolean b = new String(jalon.status).equals("ko");

            
            if(jalon.getDatej() != null && b){
              long difference = jalon.getDatej().getTime() - aujourdhui.getTime() ;
              int nbJoursDiff = (int)(difference / (1000*60*60*24));
              System.out.println(jalon.getTitle() + " Danger, nbjour=" + nbJoursDiff); 

              if (  nbJoursDiff < 1) {
                    Alerte a=new Alerte(jalon.getCai(), jalon.getTitle(), jalon.getCaiTitle(), "Danger", nbJoursDiff);
                   
                    result.add(a);
                    System.out.println(jalon.getTitle() + " Danger, nbjour=" + nbJoursDiff); 

                } else if ( nbJoursDiff < nbJoursWarning ) {
                    Alerte a=new Alerte(jalon.getCai(), jalon.getTitle(), jalon.getCaiTitle(), "Danger", nbJoursDiff);
                    result.add(a);
                    System.out.println(jalon.getTitle() + " Warning, nbjour=" + nbJoursDiff); 
                }
            }  
            
        } 
    return result;
  }

  // @GetMapping("/alertes/CAI/{id}")
  // List<Alerte> getAlerteByCAIId(Long id) {
  //   List<CAI> cais= repository.findById(id)
  //     .orElseThrow(() -> new CAINotFoundException(id));

  //   // SI status == false
  //   //date d'arrivée est dans qlq jours --> alerte jaune
  //   //date d'arrivée est dépassé --> alerte rouge
  //   Iterator<CAI> iter 
  //           = cais.iterator(); 
  //  List<Alerte> result=new ArrayList<Alerte>();
  //   while (iter.hasNext()) { 
  //         CAI  cai = iter.next();
  //           System.out.print(cai.getTitle() + " "); 
  //           if("Phase1".equals(cai.getTitle())){
  //             Alerte a=new Alerte(cai.getId(), cai.getTitle(), "Rouge", 2);
  //             result.add(a);
  //           }
  //       } 
  //       return result;
  // }



}