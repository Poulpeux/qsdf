package com.soap.springbootvue;

import com.soap.springbootvue.User;

import org.springframework.data.jpa.repository.JpaRepository;  
import org.springframework.data.rest.core.annotation.RepositoryRestResource;  
  
@RepositoryRestResource  
interface UserRepository extends JpaRepository<Todo, Long> {}