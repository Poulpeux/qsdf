package com.soap.springbootvue;

import lombok.*;
import javax.persistence.*;

import java.util.*;
  
@Entity(name = "CAI")
@Table(name = "CAI")
@Data  
@NoArgsConstructor  
@Getter @Setter
public class CAI {    
      
  @Id @GeneratedValue  
  @Column(name="cai_id")
  private Long id;  

  @NonNull
  private String title;

  private String binome;
  private String typep;  

  //@OneToMany(fetch = FetchType.EAGER,mappedBy="id", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
  //@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
  // @OneToMany(mappedBy = "cai", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
  // @OneToMany(mappedBy="cai",fetch= FetchType.EAGER)
  @OneToMany(mappedBy="cai",fetch= FetchType.EAGER,cascade = CascadeType.MERGE)
  List<CAIJalon> allTheDates= new ArrayList<CAIJalon>();
 
  // public void setAllTheDates(Date dateJalon, String TitleJalon, String DescriptionJalon){    
  //   CAIJalon itemDate = new CAIJalon(dateJalon, TitleJalon, DescriptionJalon);    
  //   this.allTheDates.add(itemDate);
  // }  
  @Override
  public int hashCode() {
        return id !=null ? 0: id.intValue() ;
  }
}