package com.soap.springbootvue;  
  
import lombok.*;  
  
import javax.persistence.Id;  
import javax.persistence.GeneratedValue;
 

import javax.persistence.Entity;  

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
  
@Entity  
@Data  
@NoArgsConstructor  
public class Todo {  
      
  @Id @GeneratedValue  
  private Long id;  

  @NonNull
  private String title;

  @JsonFormat(pattern = "yyyy-MM-dd")
  private String dateCAI;
  private String predateCAI;
  private String binome;

  private Boolean status = true;

  private Boolean completed = false;
  
}