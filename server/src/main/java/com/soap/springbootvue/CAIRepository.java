package com.soap.springbootvue;

import com.soap.springbootvue.CAI;
import java.util.*;
import com.soap.springbootvue.CAIJalon;
import org.springframework.data.jpa.repository.*; 
import org.springframework.data.rest.core.annotation.RepositoryRestResource;  
import org.springframework.transaction.annotation.Transactional;

  
@RepositoryRestResource  
interface CAIRepository extends JpaRepository<CAI, Long> {

    @Query("SELECT cai, jalon FROM CAI cai LEFT JOIN CAIJalon jalon on cai.id = jalon.cai")
    List<CAI> getAllCAI();

}