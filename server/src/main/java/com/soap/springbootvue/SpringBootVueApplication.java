package com.soap.springbootvue;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import java.util.Collections;
import java.util.stream.Stream;
import java.util.Date;
import java.text.SimpleDateFormat;

@SpringBootApplication  
public class SpringBootVueApplication {  
  
    public static void main(String[] args) {  
      SpringApplication.run(SpringBootVueApplication.class, args);  
    }  

    // Bootstrap some test data into the in-memory database
    @Bean  
    ApplicationRunner init(CAIRepository caiRepository, TodoRepository todoRepository ) {  
        return args -> {  
           
                    // Todo todo = new Todo();  
                    // todo.setTitle("Phase1");  
                    // todoRepository.save(todo);  
                    
                    // Todo toda = new Todo();  
                    // toda.setTitle("Phase2");                    
                    // todoRepository.save(toda); 
            //         SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

            //         CAI cai = new CAI();  
            //         cai.setTitle("Phase1"); 
            //         //String dateBeforeString = "23/05/2020";   
            //         //cai.setDateCAI(myFormat.parse(dateBeforeString)); 
                           
            //         caiRepository.save(cai); 

            //         CAI cai2 = new CAI();  
            //         cai2.setTitle("Phase2"); 
            //         //dateBeforeString = "20/05/2020";   
            //         // cai2.setDateCAI(myFormat.parse(dateBeforeString));   
                               
            //         caiRepository.save(cai2);     
                    
            // caiRepository.findAll().forEach(System.out::println);  
        };  
    }  

    // @Bean 
    // ApplicationRunner init(CAIRepository repository) {  
    //     return args -> {  
    //         Stream.of("Mars", "Venus", "Oter").forEach(name -> {  
    //             CAI cai = new CAI();  
    //             cai.setTitle(name);  
    //             repository.save(cai);  
    //     });  
    //     repository.findAll().forEach(System.out::println);    
    //     };  
    // }     

    // Fix the CORS errors
    @Bean
    public FilterRegistrationBean simpleCorsFilter() {  
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();  
        CorsConfiguration config = new CorsConfiguration();  
        config.setAllowCredentials(true); 
        // *** URL below needs to match the Vue client URL and port ***
        config.setAllowedOrigins(Collections.singletonList("http://localhost:8080")); 
        config.setAllowedMethods(Collections.singletonList("*"));  
        config.setAllowedHeaders(Collections.singletonList("*"));  
        source.registerCorsConfiguration("/**", config);  
        FilterRegistrationBean bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);  
        return bean;  
    }   
}