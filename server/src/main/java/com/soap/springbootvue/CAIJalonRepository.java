package com.soap.springbootvue;

import com.soap.springbootvue.CAIJalon;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.jpa.repository.JpaRepository;  
import org.springframework.data.rest.core.annotation.RepositoryRestResource;  
import org.springframework.transaction.annotation.Transactional;
  import java.util.*; 
  
@RepositoryRestResource  
interface CAIJalonRepository extends JpaRepository<CAIJalon, Long> {

    @Modifying
    @Query("DELETE FROM CAIJalon jalon WHERE jalon.cai= ?1 and jalon.id not in (?2)")
    @Transactional
    void deleteJalonByCAIId(Long caiId, List<Long> listId);

    @Modifying
    @Query("DELETE FROM CAIJalon jalon WHERE jalon.cai= ?1")
    @Transactional
    void deleteAllJalonByCAIId(Long caiId);

    @Modifying
    @Query("SELECT jalon FROM CAIJalon jalon WHERE jalon.cai= ?1")
    @Transactional
    List<CAIJalon> getJalonByCAIId(Long caiId);

    @Modifying
    @Query("DELETE FROM CAIJalon jalon WHERE jalon.id= ?1")
    @Transactional
    void deleteJalonById(Long id);
}