package com.soap.springbootvue;  
  
import lombok.*;  
import javax.persistence.Id;  
import javax.persistence.GeneratedValue;  
import javax.persistence.Entity;  
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
  
@Entity  
@Data  
@NoArgsConstructor  
public class CAIold {  
      
     
  @Id @GeneratedValue @Getter
  private Long id;  

  @NonNull  
  private String title;

  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date dateCAI;
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date predateCAI;
  private String binome;

  private Boolean status = false;

  private Boolean completed = false;



}