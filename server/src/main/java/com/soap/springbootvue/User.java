package com.soap.springbootvue;  
  
import lombok.*;  
  
import javax.persistence.Id;  
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;  
  
@Entity  
@Data  
@NoArgsConstructor  
public class User {  
      
  @Id @GeneratedValue  
  private Long id;  

  @NonNull
  private String login;
  @NonNull
  private String password;
  
}