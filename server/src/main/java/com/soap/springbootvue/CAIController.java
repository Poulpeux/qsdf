package com.soap.springbootvue;  
  
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
  import java.util.*; 

import org.springframework.web.bind.annotation.*;

 
@RestController  

public class CAIController {  
      
   private final CAIRepository repository;
   private final CAIJalonRepository jalonRepository;

  CAIController(CAIRepository repository, CAIJalonRepository jalonRepository) {
    this.repository = repository;
    this.jalonRepository=jalonRepository;
  }
   
  private List<CAI> comments = new ArrayList<>();
   
   @GetMapping("/CAIs")
  List<CAI> all() {

    return repository.getAllCAI();
  }

   @GetMapping("/jalons")
  List<CAIJalon> allCAIJalon() {
    return jalonRepository.findAll();
  }
 
  @GetMapping("/CAI/{id}")
  CAI one(@PathVariable Long id) {

    CAI cai =repository.findById(id)
      .orElseThrow(() -> new CAINotFoundException(id));
    System.out.println("cai" + cai);
    List<CAIJalon> jalons =jalonRepository.getJalonByCAIId(id);
    cai.setAllTheDates(jalons);
     return cai; 
  }

  @PostMapping("/CAI")
  CAI createCAI(@RequestBody CAI newCAI) {
        CAI c= repository.save(newCAI);
     System.out.println("post");
         List<CAIJalon> newJalons= new ArrayList<CAIJalon>();
         Iterator<CAIJalon> iter  = newCAI.getAllTheDates().iterator(); 
          while (iter.hasNext()) { 
           CAIJalon  jalon = iter.next();
            System.out.println("CAI  ID "+ c.getId());
            jalon.setCai(c.getId());
             System.out.println("jalon "+ jalon);
            CAIJalon j = jalonRepository.saveAndFlush(jalon);
            System.out.println("jalon saved"+ j);
            newJalons.add(j);
          }
         
         System.out.println(newCAI.getAllTheDates());
         c.setAllTheDates(newJalons);
        return c;
  }


@PutMapping("/CAIs/{id}")
  CAI replaceCAI(@RequestBody CAI newCAI, @PathVariable Long id) {
    
    System.out.println("*****PIZZA*****" + newCAI.getAllTheDates());

    return repository.findById(id)
      .map(cai -> {
        cai.setTitle(newCAI.getTitle());
        newCAI.setId(id);
        cai=newCAI;
        CAI c= repository.save(cai);
         
         List<CAIJalon> newJalons= new ArrayList<CAIJalon>();
         List<Long> listId= new ArrayList<Long>();
         Iterator<CAIJalon> iter  = newCAI.getAllTheDates().iterator(); 
          while (iter.hasNext()) { 
           CAIJalon  jalon = iter.next();
            jalon.setCai(id);
            jalon.setCaiTitle(cai.getTitle());
            CAIJalon j = jalonRepository.save(jalon);
            newJalons.add(j);
            listId.add(j.getId());
          }
         
        jalonRepository.deleteJalonByCAIId(id, listId);

         System.out.println(newCAI.getAllTheDates());
         c.setAllTheDates(newJalons);
        return c;
      })
      .orElseGet(() -> {
        newCAI.setId(id);
        return repository.save(newCAI);
      });
  }

  @DeleteMapping("/jalon/{id}")
  void deleteJalon(@PathVariable Long id) {
  System.out.println("******Delete Id="+ id);
  jalonRepository.deleteJalonById(id);
  // return jalonRepository.findAll();
  }
  
@DeleteMapping("CAI/{id}/jalons")
  void deleteJalons(@PathVariable Long id) {
    System.out.println("Delete Id="+ id);
    jalonRepository.deleteAllJalonByCAIId(id);
   // return jalonRepository.findAll();
  }

  @GetMapping("CAI/{id}/jalons")
   List<CAIJalon> getJalonsByCAIId(@PathVariable Long id) {
    System.out.println("Delete Id="+ id);
    return jalonRepository.getJalonByCAIId(id);
   // return jalonRepository.findAll();
  }

  @PostMapping("liloulo")
  CAIJalon createJalon(@RequestBody CAIJalon jalon) {
  System.out.println("*************************************create jalon");
  CAIJalon j = jalonRepository.saveAndFlush(jalon);
  System.out.println("----------------------------------create created");
  return j;
  }


}