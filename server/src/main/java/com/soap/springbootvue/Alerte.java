package com.soap.springbootvue;  
 
import lombok.*; 


@Data 
 public class Alerte{
      //CAI Id
      Long caiId;
      
      //CAI Title
      String caiTitle;
      //CAI Title
      String titi;
      //Type d'alerte : jaune ou rouge
      String typeAlerte;
      //nombre de jours de retard
      int nbJours;

      Alerte(Long caiId, String caiTitle, String titi, String typeAlerte, int nbJours){
        this.caiId = caiId;
        this.caiTitle = caiTitle;
        this.titi = titi;
        this.typeAlerte = typeAlerte;
        this.nbJours = nbJours;

      }
   }
